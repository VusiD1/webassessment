package ElementAndAction;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.devtools.Runtime;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import java.util.concurrent.TimeUnit;

public class Add_New_User {
    /**
     This class contains all the elements to locate the objects and all their actions methods, which is then called on the tests method.
     */

    WebDriver driver;

    public Add_New_User(WebDriver driver)
    {
        this.driver=driver;
    }

//  Regular expression is used to locate the elements below

    @FindBy(xpath = "/html//table[@class='smart-table table table-striped']")
    WebElement UserTable;

    @FindBy(how =How.XPATH,using = "//button[contains(.,'Add User')]")
    WebElement AddUserButton;

    @FindBy(how =How.XPATH,using = "//input[contains(@name,'FirstName')]")
    WebElement FirstName;

    @FindBy(how =How.XPATH,using  = "//input[contains(@name,'LastName')]")
    WebElement LastName;

    @FindBy(how =How.XPATH,using = "//input[contains(@name,'UserName')]")
    WebElement UserName;

    @FindBy(how =How.XPATH,using = "//input[contains(@name,'Password')]")
    WebElement Password;

    @FindBy(how =How.XPATH,using  = "//label[contains(.,'Company AAA')]")
    WebElement CustomerCompanyAA;

    @FindBy(how =How.XPATH,using  = "//label[contains(.,'Company BBB')]")
    WebElement CustomerCompanyBB;

    @FindBy(how =How.XPATH,using = "//select[contains(@name,'RoleId')]")
    WebElement Role;

    @FindBy(how =How.XPATH,using  = "//input[contains(@type,'email')]")
    WebElement Email;

    @FindBy(how =How.XPATH,using  = "//input[contains(@name,'Mobilephone')]")
    WebElement MobilePhone;

    @FindBy(how =How.XPATH,using  = "//button[@ng-click='save(user)'][contains(.,'Save')]")
    WebElement Button_Save;

    @FindBy(how =How.XPATH,using  = "(//td[contains(@class,'smart-table-data-cell')])[1]")
    WebElement First_Row_Data;

  /*on each of the actions below, I have included the check that checks if the element
  is available before it can take any action, this will help to minimise the number of failures due to the slow network or any other reason*/
    public void Verify_User_Table_Displayed() {
//      assigned a variable so that i can use it on the assertion
        WebElement results =new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(UserTable));
        Assert.assertTrue(results.isDisplayed()); // this assertion is verifying that the table element is available on the page
    }

    public void Click_Add_New_User_Button() {
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(AddUserButton)).click();
    }
    public void Populate_First_Name(String firstname) {
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(FirstName));
        FirstName.clear();
        FirstName.sendKeys(firstname);
    }

    public void Populate_Last_Name(String lastname) {
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(LastName));
        LastName.clear();
        LastName.sendKeys(lastname);
    }
    public void Populate_UserName(String username) {
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(UserName));
        Runtime.Timestamp timestamp = new Runtime.Timestamp(System.currentTimeMillis());  // I am appending this to a user name to create a unique one
        username+=timestamp;
        UserName.clear();
        UserName.sendKeys(username);
    }
    public void Populate_Password(String password) {
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(Password));
        Password.clear();
        Password.sendKeys(password);
    }
    public void SelectCustomerCompanyAA() {
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(CustomerCompanyAA)).click();
    }

    public void SelectCustomerCompanyBB() {
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(CustomerCompanyBB)).click();
    }

    public void Select_Role(String role) {
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(Role)).sendKeys(role);
    }
    public void Populate_Email(String email) {
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(Email));
        Email.clear();
        Email.sendKeys(email);
    }
    public void Populate_Mobile_Phone(String mobilenumber) {
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(MobilePhone));
        MobilePhone.clear();
        MobilePhone.sendKeys(mobilenumber);
    }
    public void Click_Button_Save() {
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(Button_Save)).click();
    }
    public void Verify_Data_Saved(String saved_firstname) throws InterruptedException {
        TimeUnit.SECONDS.sleep(2);
        Assert.assertEquals(First_Row_Data.getText(),saved_firstname); // Passing the parameter to be used for comparison on the test class
    }

}
