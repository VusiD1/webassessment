### Absa Web Automation -Java Framework

This page covers step by step instruction for installing and running Selenium Web automated test

### IDE to be used
##### As a dev you can use any java IDE of your preference however the following is recommended

1. ### Intellij
2. ### Eclipse

### Step by step Installation instructions

#### Java 8 is to be installed on the following site
https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html

#### Intellij can be downloaded from the following site
https://www.jetbrains.com/idea/download

#### Eclipse can be downloaded from the following site
https://www.eclipse.org/downloads/

### Dependencies
##### the project is maven project , so you don't need to worry about downloading any separate dependencies, all the required dependencies neccessary for this project is already included on the project , you can review them on the Pom.xm file included in the project.

### Project Structure(important folders and Packages explanatory ) 

1. ##### Configuration- in this Package there is nothing to change or update before running just keep everything as is, it contains setups---found in src/test/java
2. ##### ElementAndAction- This Package contains java classes with elements locators and its methods named accordingly(This are the classes you will need to update if some elements change in the UI or if you need to add more tests)---found in src/test/java
3. ##### Tests- This Package contains test classes(Additional test cases will be added in this folder to accommodate new requirements )---found in src/test/java
4. ##### TestData-This folder contains the excel file that has the username and password inputs.
5. ##### Driver-This Folder contains the Drivers for different web browsers.
6. ##### POM.xml - This file contains all the dependencies required in the project which are already included.
7. ##### testng.xml - This file contains all the test classes in case you want to run all of them at the same time(In this project there is only one)
8. ##### External Libraries- Here you will find all the maven dependencies after it has downloaded successful.
9. ##### Reports Folder - At the end of the execution, the report will be created and stored on this folder-I used the latest extent report to build the report class

### Executing the tests
##### The tests are designed to be executed independently and also using the testng.xml located in the project you will be able to execute all test classes on a single click(in this case there is only one test class)
1. ##### To execute the tests independently all you need to do is open  any test class and click run button(by default it will run as Testng because I have included dependencies for TestNG)
2. ##### To run all the test classes at the same time, open the testng.xml file and then run it, it will run all the listed tests in their order accordingly,
3. ##### Before you run all your tests, Make sure all the dependencies are downloaded successful(Can be confirmed under External Libraries)
4. ##### Make sure you have the browser specified in Add_New_User_Tests class installed on your machine.


### Executing the Tests on command line Locally
##### https://www.youtube.com/watch?v=lQCyVy-g1e8

### Executing the Tests on Jenkins  Locally
##### https://www.youtube.com/watch?v=no98HueS3Ws

### Sending an email on jenkins after execution
##### https://www.youtube.com/watch?v=HVQm3kdp9bA

### Jenkins integration with bitbucket
##### https://www.youtube.com/watch?v=SSxZYKea700
























