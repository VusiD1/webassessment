package Configuration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class BrowserSetup {

    /**
     This is the browser setup class there , only two browser drivers are included for now, if need to add more class you will need to configure in this class
     */

    public static String dir = System.getProperty("user.dir");
    public static final String Chromedriver = dir+"/Drivers/chromedriver.exe"; // Cromedriver location
    public static final String Firefox = dir+"/Drivers/geckodriver.exe"; //Geckodriver location

    static WebDriver driver;

//  The below is the browser choice, method, i have configured the tests to run on only two browsers for now, I can add more by just extending the if statement
    public static WebDriver startBrowser(String browserchoice,String url) {
        if ("firefox".equals(browserchoice)) {
            System.setProperty("webdriver.gecko.driver", Firefox);
            driver = new FirefoxDriver();
        } else if ("chrome".equals(browserchoice)) {
            System.setProperty("webdriver.chrome.driver", Chromedriver);
            driver = new ChromeDriver();
        }
        driver.manage().window().maximize(); // the maximize the browser to full screen
        driver.get(url);
        return driver;
    }
}
