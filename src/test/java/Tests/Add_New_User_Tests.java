package Tests;

import Configuration.BrowserSetup;
import Configuration.ExtentReportClass;
import ElementAndAction.Add_New_User;
import com.aventstack.extentreports.Status;
import org.apache.commons.io.FileUtils;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class Add_New_User_Tests extends ExtentReportClass {

    /**
     This is the tests class, where all the actions are being executed, I have specified  the proirities of all the test methods in this class
     so that it can know which one to execute first and last.
     */

    public static String dir = System.getProperty("user.dir");
    public static final String excel = dir+"/TestData/DataSheet.xlsx";

    //To use the different browser change the browser choice, details can be found on BrowserSetup class
    WebDriver driver = BrowserSetup.startBrowser("chrome","http://www.way2automation.com/angularjs-protractor/webtables/");

    Add_New_User add_new_user = PageFactory.initElements(driver, Add_New_User.class);

    @Test (priority = 1,description = "Verify that User Table is displayed")
    public void Verify_User_Table_Present() {
        test = extent.createTest("Table Loaded Verification ");
        test.log(Status.INFO, "Verify User Table Test Started");
        add_new_user.Verify_User_Table_Displayed();
        test.log(Status.INFO, "Verify User Table Tests Completed");
    }

    @Test(priority = 2,description = "Add the user to the User Table")
    public void Add_First_User() throws IOException {

        test = extent.createTest("Add First User to the Table ");
        test.log(Status.INFO, "Add First User To User Table Test Started");

//      The below is getting the data from the excel file and input to the user fields
        FileInputStream fis = new FileInputStream(excel);
        XSSFWorkbook workbook = new XSSFWorkbook(fis);
        XSSFSheet sheet = workbook.getSheetAt(0);
        String first_name = sheet.getRow(1).getCell(0).getStringCellValue();
        String last_name = sheet.getRow(1).getCell(1).getStringCellValue();
        String user_name = sheet.getRow(1).getCell(2).getStringCellValue();
        String pass_word = sheet.getRow(1).getCell(3).getStringCellValue();
        String email = sheet.getRow(1).getCell(4).getStringCellValue();
        String cellphone = sheet.getRow(1).getCell(5).getStringCellValue();

//       the below code add the first user to the table
        add_new_user.Click_Add_New_User_Button();
        add_new_user.Populate_First_Name(first_name);
        add_new_user.Populate_Last_Name(last_name);
        add_new_user.Populate_UserName(user_name);
        add_new_user.Populate_Password(pass_word);
        add_new_user.SelectCustomerCompanyAA();
        add_new_user.Select_Role("Admin");
        add_new_user.Populate_Email(email);
        add_new_user.Populate_Mobile_Phone(cellphone);
        add_new_user.Click_Button_Save();
        getScreenshot(driver);

        test.log(Status.INFO, "Add First User To User Table Test Completed");
    }

    @Test(priority = 3,description = "Verify that the First User is added correctly to the table")
    public void Verify_First_User_Data_Saved() throws IOException, InterruptedException {

        test = extent.createTest("Test To Verify First User Data Was Added To The User Table ");
        test.log(Status.INFO, "Verify First User Added Test Started");
        FileInputStream fis = new FileInputStream(excel);
        XSSFWorkbook workbook = new XSSFWorkbook(fis);
        XSSFSheet sheet = workbook.getSheetAt(0);

        String first_name = sheet.getRow(1).getCell(0).getStringCellValue();// getting the first raw data from the excel to compare with the one on the table
        add_new_user.Verify_Data_Saved(first_name);

        test.log(Status.INFO, "Verify First User Added Test Completed Successfully");
    }

    @Test(priority = 4,description = "Add the Second user to the User Table")
    public void Add_Second_User() throws IOException {

        test = extent.createTest("Add the Second User to the Table");
        test.log(Status.INFO, "Add The Second User To the Table Tests Started");

//      The below is getting the data from the excel file and input to the user fields
        FileInputStream fis = new FileInputStream(excel);
        XSSFWorkbook workbook = new XSSFWorkbook(fis);
        XSSFSheet sheet = workbook.getSheetAt(0);
        String first_name = sheet.getRow(2).getCell(0).getStringCellValue();
        String last_name = sheet.getRow(2).getCell(1).getStringCellValue();
        String user_name = sheet.getRow(2).getCell(2).getStringCellValue();
        String pass_word = sheet.getRow(2).getCell(3).getStringCellValue();
        String email = sheet.getRow(2).getCell(4).getStringCellValue();
        String cellphone = sheet.getRow(2).getCell(5).getStringCellValue();

        // the below code add the second user to the table
        add_new_user.Click_Add_New_User_Button();
        add_new_user.Populate_First_Name(first_name);
        add_new_user.Populate_Last_Name(last_name);
        add_new_user.Populate_UserName(user_name);
        add_new_user.Populate_Password(pass_word);
        add_new_user.SelectCustomerCompanyBB();
        add_new_user.Select_Role("Customer");
        add_new_user.Populate_Email(email);
        add_new_user.Populate_Mobile_Phone(cellphone);
        add_new_user.Click_Button_Save();
        getScreenshot(driver);
        test.log(Status.INFO, "Add The Second User To the Table Tests Completed");
    }
//  The below test method verifies the record entered on the table
    @Test(priority = 5,description = "Verify that the Second User is added to the table")
    public void Verify_Second_User_Data_Saved() throws IOException, InterruptedException {

        test = extent.createTest("Test To Verify Second User Data Was Added To The User Table ");
        test.log(Status.INFO, "Verify Second User Added Test Started");

        FileInputStream fis = new FileInputStream(excel);
        XSSFWorkbook workbook = new XSSFWorkbook(fis);
        XSSFSheet sheet = workbook.getSheetAt(0);

        String first_name = sheet.getRow(2).getCell(0).getStringCellValue(); // getting the second raw data from the excel to compare with the one on the table
        add_new_user.Verify_Data_Saved(first_name);

        test.log(Status.INFO, "Verify Second User Added Test Completed Successfully");
    }
//  The below method is the screenshot method which i call after each raw is saved(this saves as a proof that the data was successful saved to the table)
    public static String getScreenshot(WebDriver driver)
    {
        TakesScreenshot ts =(TakesScreenshot) driver;
        File src = ts.getScreenshotAs(OutputType.FILE);

        String path =System.getProperty("user.dir")+"/Screenshots/"+System.currentTimeMillis()+".png";

        File destination = new File(path);

        try {
            FileUtils.copyFile(src,destination);
        }catch (IOException e)
        {
            System.out.println("Capture Failed "+e.getMessage());
        }
        return path;
    }

    //The below method closes the browser at the end of tests execution
    @AfterClass
    public void closeBrowser() {
        driver.quit();
    }

}
